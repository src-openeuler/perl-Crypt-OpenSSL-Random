Name:            perl-Crypt-OpenSSL-Random
Version:         0.17
Release:         2
Summary:         LibreSSL pseudo-random number generator access
License:         GPL-1.0-or-later OR Artistic-1.0-Perl
Group:           Development/Libraries
URL:             https://metacpan.org/release/Crypt-OpenSSL-Random
Source0:         https://cpan.metacpan.org/authors/id/R/RU/RURBAN/Crypt-OpenSSL-Random-%{version}.tar.gz
BuildRequires:   gcc openssl openssl-devel coreutils findutils make
BuildRequires:   perl-devel perl-generators perl-interpreter perl(Config)
BuildRequires:   perl(Crypt::OpenSSL::Guess) >= 0.11
BuildRequires:   perl(Exporter) perl(ExtUtils::MakeMaker) perl(strict)
BuildRequires:   perl(Test::More) perl(vars) perl(XSLoader)

%{?perl_default_filter}

%description
"Crypt::OpenSSL::Random" provides the ability to seed and query the
OpenSSL and LibreSSL library's pseudo-random number generators.

%package_help

%prep
%autosetup -n Crypt-OpenSSL-Random-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install

%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENSE
%doc Changes
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/Crypt/

%files help
%{_mandir}/man3/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 0.17-2
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Nov 28 2024 yaoxin <yao_xin001@hoperun.com> - 0.17-1
- Update to 0.17:
  * fix older aix with missing -lz dependencies (timlegge PR #16)
  * minor ci fixes
  * add github actions, travis and appveyor.
  * Many patches by Takumi Akiyama.
  * Fix broken github image for strawberry perl by stripping its PATH. their
    new mingw is incompatible to their old strawberry 5.32
  * minor documentation fixes.
- License compliance rectification

* Wed Nov 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.15-4
- Package init
